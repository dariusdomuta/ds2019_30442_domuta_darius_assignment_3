import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends GetDataImpl {
    public Server() {}
    public static void main(String args[]) {
        try {
            // Instantiating the implementation class
            GetDataImpl obj = new GetDataImpl();

            // Exporting the object of implementation class (
            //here we are exporting the remote object to the stub)
            GetDataRmi stub = (GetDataRmi) UnicastRemoteObject.exportObject(obj, 0);

            // Binding the remote object (stub) in the registry
            Registry registry = LocateRegistry.getRegistry();

            registry.bind("GetDataRmi", stub);
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
