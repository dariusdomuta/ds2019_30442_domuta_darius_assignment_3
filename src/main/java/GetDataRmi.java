import java.rmi.Remote;
import java.util.List;

public interface GetDataRmi extends Remote {
    public List<MedicationPlan> getMedicationPlans() throws Exception;
}
