import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class Client extends Application{
    //private Client() {}

    private static List<MedicationPlan> medicationPlans;

    public static void main(String[] args)throws Exception {
        try {
            // Getting the registry
            Registry registry = LocateRegistry.getRegistry(null);

            // Looking up the registry for the remote object
            GetDataRmi stub = (GetDataRmi) registry.lookup("GetDataRmi");

            // Calling the remote method using the obtained object
            List<MedicationPlan> list = (List<MedicationPlan>)stub.getMedicationPlans();
            medicationPlans = list;
            for (MedicationPlan medicationPlan: list ) {

                System.out.println("ID: " + medicationPlan.getMedicationPlanId());
                System.out.println("Medication name: " + medicationPlan.getMedicationName());
                System.out.println("Side Effects: " + medicationPlan.getSideEffects());
                System.out.println("Dosage: " + medicationPlan.getDosage());
                System.out.println("Minimum intake hour: " + medicationPlan.getMinIntakeHour());
                System.out.println("Maximum intake hour: " + medicationPlan.getMaxIntakeHour());
                System.out.println("Number of days: " + medicationPlan.getNumberOfDays());
            }

            launch();

            // System.out.println(list);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        TableView table;
        Text actionStatus;

        ObservableList data = FXCollections.observableList(medicationPlans);

        primaryStage.setTitle("Medication Plan");

        Label timeLabel = new Label();
        DateFormat timeFormat = new SimpleDateFormat( "HH:mm:ss" );
        final Timeline timeline = new Timeline(
                new KeyFrame(
                        Duration.millis( 500 ),
                        event -> {
                            final long diff = System.currentTimeMillis();
                            if ( diff < 0 ) {
                                //  timeLabel.setText( "00:00:00" );
                                timeLabel.setText( timeFormat.format( 0 ) );
                            } else {
                                timeLabel.setText( timeFormat.format( diff ) );
                            }
                        }
                )
        );
        timeline.setCycleCount( Animation.INDEFINITE );
        timeline.play();

        // Medication label
        Label label = new Label("Medication Plan");
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().add(timeLabel);

        // Table view, data, columns and properties

        table = new TableView();

        table.setItems(data);

        TableColumn idCol = new TableColumn("Id");
        idCol.setCellValueFactory(new PropertyValueFactory("medicationPlanId"));
        TableColumn medicationCol = new TableColumn("Medication");
        medicationCol.setCellValueFactory(new PropertyValueFactory("medicationName"));
        TableColumn sideEffectsCol = new TableColumn("Side effects");
        sideEffectsCol.setCellValueFactory(new PropertyValueFactory("sideEffects"));
        TableColumn dosageCol = new TableColumn("Dosage");
        dosageCol.setCellValueFactory(new PropertyValueFactory("dosage"));
        TableColumn minIntakeHourCol = new TableColumn("Min Intake Hour");
        minIntakeHourCol.setCellValueFactory(new PropertyValueFactory("minIntakeHour"));
        TableColumn maxIntakeHourCol = new TableColumn("Max Intake Hour");
        maxIntakeHourCol.setCellValueFactory(new PropertyValueFactory("maxIntakeHour"));
        TableColumn numberOfDaysCol = new TableColumn("Number of Days");
        numberOfDaysCol.setCellValueFactory(new PropertyValueFactory("numberOfDays"));

        table.getColumns().setAll(idCol, medicationCol, sideEffectsCol, dosageCol, minIntakeHourCol, maxIntakeHourCol, numberOfDaysCol);
        table.setPrefWidth(450);
        table.setPrefHeight(300);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // Status message text
        actionStatus = new Text();
        actionStatus.setFill(Color.FIREBRICK);

        // Vbox
        VBox vbox = new VBox(20);
        vbox.setPadding(new Insets(25, 25, 25, 25));;
        vbox.getChildren().addAll(hb, table, actionStatus);

        // Scene
        Scene scene = new Scene(vbox, 500, 475); // w x h
        primaryStage.setScene(scene);
        primaryStage.show();
    }


}
