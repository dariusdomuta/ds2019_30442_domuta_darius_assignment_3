import java.io.Serializable;

public class MedicationPlan implements Serializable {
    private int medicationPlanId;
    private String medicationName;
    private String sideEffects;
    private int dosage;
    private int minIntakeHour;
    private int maxIntakeHour;
    private int numberOfDays;

    public int getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(int medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public int getMinIntakeHour() {
        return minIntakeHour;
    }

    public void setMinIntakeHour(int minIntakeHour) {
        this.minIntakeHour = minIntakeHour;
    }

    public int getMaxIntakeHour() {
        return maxIntakeHour;
    }

    public void setMaxIntakeHour(int maxIntakeHour) {
        this.maxIntakeHour = maxIntakeHour;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
}
