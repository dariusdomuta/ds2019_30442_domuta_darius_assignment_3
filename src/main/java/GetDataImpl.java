import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetDataImpl implements GetDataRmi {

    // Implementing the interface method
    public List<MedicationPlan> getMedicationPlans() throws Exception {
        List<MedicationPlan> list = new ArrayList<MedicationPlan>();

        // JDBC driver name and database URL
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://localhost:3306/medication-platform";

        // Database credentials
        String USER = "root";
        String PASS = "";

        Connection conn = null;
        Statement stmt = null;

        //Register JDBC driver
        Class.forName("com.mysql.jdbc.Driver");

        //Open a connection
        System.out.println("Connecting to a selected database...");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        System.out.println("Connected database successfully...");

        //Execute a query
        System.out.println("Creating statement...");

        stmt = conn.createStatement();
        String sql = "SELECT * FROM medication_plan";
        ResultSet rs = stmt.executeQuery(sql);

        //Extract data from result set
        while(rs.next()) {
            // Retrieve by column name
            int id  = rs.getInt("medicationPlanId");

            String medicationName = rs.getString("medicationName");
            String sideEffects = rs.getString("sideEffects");
            int dosage = rs.getInt("dosage");
            int minIntakeHour = rs.getInt("minIntakeHour");
            int maxIntakeHour = rs.getInt("maxIntakeHour");
            int numberOfDays = rs.getInt("numberOfDays");


            // Setting the values
            MedicationPlan medicationPlan = new MedicationPlan();
            medicationPlan.setMedicationPlanId(id);
            medicationPlan.setMedicationName(medicationName);
            medicationPlan.setSideEffects(sideEffects);
            medicationPlan.setDosage(dosage);
            medicationPlan.setMinIntakeHour(minIntakeHour);
            medicationPlan.setMaxIntakeHour(maxIntakeHour);
            medicationPlan.setNumberOfDays(numberOfDays);
            list.add(medicationPlan);
        }
        rs.close();


        return list;
    }
}